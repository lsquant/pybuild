from decimal import InvalidOperation
from typing import Type
from unicodedata import name
import numpy as np
import matplotlib.pyplot as plt
import os
import pybinding as pb
import json


#Disorder Classes

class Anderson(dict):
    """
        Class to model Anderson type of disorder
        
        Parameters
        ----------

        U   :   float
            Strenght of the Anderson disorder that will be chosen from [-U/2,U/2]
        
        Sublats : list of strings
            Name of the sublattices that will have Anderson disorder included
        
        Mat : 1d array
            List of integers that specify which orbitals belong to a single site

    
    
    """
    def _init_ (self, U=None, Sublats = None, Mat = None):
        self._pars = {"U": U, "Sublats":Sublats, "Mat":Mat}#las matrices aca tendran los labesl de las redes diferentes

    @property
    def getU(self):
        return self._pars["U"]
    @property
    def getSublats(self):
        return self._pars["Sublats"]
    @property
    def getMat(self):
        return self._pars["Mat"]

class Puddle(dict):

    """
        Class to model electron-hole puddles
        
        Parameters
        ----------

        U   :   float
            Strenght of the disorder that will be chosen from [-U/2,U/2]
        
        Sublats : list of strings
            Name of the sublattices that will have Anderson disorder included
        
        Sigma : float
            Variance of the gaussian function, it relates the disorder decay lenght
        
        X : float
            Concentration of puddles in the system

        Mat : 1d array
            List of integers that specify which orbitals belong to a single site

    
    
    """


    def _init_(self, U=None, Sublats=None, Sigma = None, X = None, Mat = None):
        self._pars ={"U": U, "Sublats":Sublats, "Sigma": Sigma, "X": X, "Mat": Mat}
    @property
    def getU(self):
        return self._pars["U"]
    @property
    def getSublats(self):
        return self._pars["Sublatss"]
    @property
    def getSigma(self):
        return self._pars["Sigma"]
    @property
    def getX(self):
        return self._pars["X"]
    
    @property
    def getMat(self):
        return self._pars["Mat"]

class Vacancy(dict):
    """
        Class to model Vacancies
        
        Parameters
        ----------

        Sublats : list of strings
            Name of the sublattices that will have Anderson disorder included
        
        X : float
            Concentration of puddles in the system

        Mat : 1d array
            List of integers that specify which orbitals belong to a single site

    
    
    """

    def _init_(self, Sublats=None, X=None, Mat=None):
        self._pars={"Sublats":Sublats, "X":X, "Mat": Mat}
    @property
    def getSublats(self):
        return self._pars["Sublats"]
    @property
    def getX(self):
        return self._pars["X"]
    @property
    def getMat(self):
        return self._pars["Mat"]

class Unitary:
    """
        Class that contains all the information related to the Unit lattices
        
        Parameters
        ----------

        name : string
            Name of the lattice, this will be the prefix of the configuration files folder
        
        vec1 : 1d array of floats 
            lattice vector 1
        
        vec2 : 1d array of floats 
            lattice vector 2

        vec3 : 1d array of floats 
            lattice vector 3

        Note: if v3 is set to null pybinding will treat the system as a 2D lattice

    
    """
    def __init__(self,name="DefaultLatName",vec1=None, vec2=None, vec3=None):
        self._name = name
        self._vec1 = vec1
        self._vec2 = vec2
        self._vec3 = vec3
        self._PBlat = None
        self._PBOperators = None ## This will be a dict of pybinding objects that will be defined in the future
        self._FuncHoppings=None
        self._FuncSublat=None
        
        self._AndersonDisorder=None
        self._PuddleDisorder=None
        self._VacancyDisorder =None
        

    
    #### Now we have to initialize the lattice

    def BuildLat(self) -> None:
        if(self._vec3 == None):
            self._PBlat = pb.Lattice(self._vec1,self._vec2)### Taking the first two coordinates for the vector
        else:
            self._PBlat = pb.Lattice(self._vec1,self._vec2,self._vec3)### Taking the first two coordinates for the vector

    def SetOrbitals(self, *orbitals) -> None:
        self._PBlat.add_sublattices(*orbitals)
        ###Now if everything is well this should work without any problem
    
    def SetTimeDepOrbitals(self,*orbitals) -> None:
        if(not self._FuncSublat):
            self._FuncSublat = []
        for f in orbitals:
            self._FuncSublat.append(f)
        self._PBlat.add_sublattices(*orbitals)

    def SetOrbital(self, orbital) -> None:
        self._PBlat.add_one_sublattice(*orbital)

    def SetTimeDepOrbital(self, orbital) -> None:
        if(not self._FuncSublat):
            self._FuncSublat = []
        self._FuncSublat.append(orbital)
        self._PBlat.add_one_sublattice(*orbital)

    def SetHoppings(self, *hoppings) -> None:
        self._PBlat.add_hoppings(*hoppings)

    def SetTimeDepHoppings(self, *hoppings)-> None:
        if(not self._FuncHoppings):
            self._FuncHoppings = []
        for f in hoppings:
            self._FuncHoppings.append(f)
        self._PBlat.add_hoppings(*hoppings)


    def SetHopping(self, hopping) -> None:
        self._PBlat.add_one_hopping(*hopping)

    def SetTimeDepHopping(self, hopping) ->None:
        if(not self._FuncHoppings):
            self._FuncHoppings =[]
        self._FuncHoppings.append(hopping)
        self._PBlat.add_one_hopping(*hopping)

    ### Now we create the opeartors
    
    def CreateOperator(self,name : str) -> None:
        if(not self._PBOperators):
            self._PBOperators= {}
        if(self._vec3):
            if(self._PBOperators.get(name,None)):
                raise KeyError("The operator already exists")
            else:
                self._PBOperators[name]= pb.Lattice(self._vec1,self._vec2, self._vec3)

        else:
            if(self._PBOperators.get("name",None)):
                raise KeyError("The operator already exists")
            else:
                self._PBOperators[name]= pb.Lattice(self._vec1,self._vec2)
    ### Now we need to define the methods for the operator
    
    def SetOperatorOrbitals(self,name, *orbitals) -> None:
        if(not (self._PBOperators or self._PBOperators.get(name,None))):
            raise KeyError("Before setting the orbitals of the operators these operators have to be declared ")
        else:
            for i,f in enumerate(orbitals):
                if(not self._PBlat.sublattices.get(f[0],None)):
                    print(f)
                    raise ValueError("The sublattice added in the {} orbital of the tuple isn't in the original lattice ".format(i))
                if(np.array_equal(f[1],self._PBlat.sublattices[f[0]].position)):
                    raise ValueError("The sublattice added in the {} orbital of the tuple is not at the same position of the same sublattice in the original lattice".format(i))
                if(np.asarray(f[2]).shape != self._PBlat.sublattices[f[0]].energy.shape):
                    raise ValueError("The number of orbitals added in the {} orbital does not match the orbital size of the original lattices ".format(i))
                self._PBOperators[name].add_one_sublattice(*f)
    
    def SetOperatorHoppings(self, name, *hoppings) -> None:
        if(not (self._PBOperators or self._PBOperators.get(name,None))):
            raise KeyError("Before setting the orbitals of the operators these operators have to be declared ")
        else:
            for i,f in enumerate(hoppings):
                if(not self._PBlat.sublattices.get(f[1],None) or not self._PBlat.sublattices.get(f[2],None) ):
                    raise ValueError("The one of the sublattices added in the {} Hopping of the tuple isn't in the original lattice ".format(i))
                if(np.linalg.norm(f[0])>0.):
                    raise ValueError("Error: The relative index of the Hopping {} of the tuple goes beyond the unit cell".format(i))

                self._PBOperators[name].add_one_hopping(*f)



    def AddAnderson(self, Disorder):
        if(not isinstance(Disorder,Anderson)):
            raise TypeError("Error: The disorder passed to the method is not an instance of the Anderson Class\n")

        if(not self._AndersonDisorder):
            self._AndersonDisorder = []
        self._AndersonDisorder.append(Disorder)
    
    def AddMultipleAnderson(self, *Disorder):
        if(not all(isinstance(x, Anderson) for x in Disorder)):
            raise TypeError("Error: The disorder passed to the method is not an instance of the Anderson Class\n")
        if(not self._AndersonDisorder):
            self._AndersonDisorder = []
        for f in Disorder:
            self._AndersonDisorder.append(f)

    def AddPuddle(self, Disorder):
        if(not isinstance(Disorder,Puddle)):
            raise TypeError("Error: The disorder passed to the method is not an instance of the Puddle Class\n")
        if(not self._PuddleDisorder):
            self._PuddleDisorder = []
        self._PuddleDisorder.append(Disorder)
    
    def AddMultiplePuddle(self, *Disorder):
        if(not all(isinstance(x, Puddle) for x in Disorder)):
            raise TypeError("Error: The disorder passed to the method is not an instance of the Puddle Class\n")
        if(not self._PuddleDisorder):
            self._PuddleDisorder = []
        for f in Disorder:
            self._PuddleDisorder.append(f)

    def AddVacancy(self, Disorder):
        if(not isinstance(Disorder,Vacancy)):
            raise TypeError("Error: The disorder passed to the method is not an instance of the Vacancy Class\n")
        if(not self._VacancyDisorder):
            self._VacancyDisorder = []
        self._VacancyDisorder.append(Disorder)
    
    def AddMultipleVacancy(self, *Disorder):
        if(not all(isinstance(x,Vacancy) for x in Disorder)):
            raise TypeError("Error: The disorder passed to the method is not an instance of the Vacancy Class\n")
        if(not self._VacancyDisorder):
            self._VacancyDisorder=[]
        for f in Disorder:
            self._PuddleDisorder.append(f)

    
    def CreateFolder(self):
        if(not os.path.exists("./"+self._name+".prop")):
            os.makedirs("./"+self._name+".prop")
            print("Property Directory Created!")
        else:
            pass
    
    def FlushLatticeBasics(self):
        if(not self._vec3):
            output = {"name":self._name, "Vec1":self._vec1, "Vec2":self._vec2, "Vec3":self._vec3}
        else:
            output = {"name":self._name, "Vec1":self._vec1, "Vec2":self._vec2}
        with open("./"+self._name+".prop/LatticeBasics.json","w") as f:
            json.dump(output,f)
    ####Falta Por debuguear
    def FlushSublattices(self):
        output ={"Sublattices":[]}
        if(not self._FuncSublat):
            for name, sublat in self._PBlat.sublattices.items():
                output["Sublattices"].append({"Label":name, "Position":np.array(sublat.position[0:len(self._vec1)],dtype=float).tolist(), "Nrows":sublat.energy.shape[0], "Ncols":sublat.energy.shape[1], "OnsiteReal":np.real(sublat.energy.ravel()).tolist(),"OnsiteImag":np.imag(sublat.energy.ravel()).tolist(), "Kind":"Plain"})
        else:
            
            for f in self._FuncSublat:
                try:
                    onsite = f[2]
                except IndexError:
                    onsite = 0
                    output["Sublattices"].append({"Label":f[0], "Position":f[1], "Nrows":1, "Ncols":1, "OnsiteReal":[0],"OnsiteImag":[0],  "Kind":"Function"})

                else:
                    if(not isinstance(f[2],np.ndarray)):
                        output["Sublattices"].append({"Label":f[0], "Position":f[1], "Nrows":np.array([f[2]]).shape[0], "Ncols":np.array([f[2]]).shape[0], "OnsiteReal":np.real(np.array([f[2]]).ravel()).tolist(),"OnsiteImag":np.imag(np.array([f[2]]).ravel()).tolist(), "Kind":"Function"})
                    else:
                        output["Sublattices"].append({"Label":f[0], "Position":f[1], "Nrows":np.array(f[2]).shape[0], "Ncols":np.array(f[2]).shape[0], "OnsiteReal":np.real(np.array(f[2] ).ravel()).tolist(), "OnsiteImag":np.imag(np.array(f[2] ).ravel()).tolist(), "Kind":"Function"})

        with open("./"+self._name+".prop/Sublattices.json","w") as f:
            json.dump(output,f,indent=4)
    
    ### Flush Hoppings
    def FlushHoppings(self):
        output = {"Hoppings":[]}
        keymap ={}
        #first we iterate over the sublatatices to get the map values
        for name, sub in self._PBlat.sublattices.items():
            if(not keymap.get(str(sub.unique_id),None)):
                keymap[str(sub.unique_id)] = name
        
        if( not self._FuncHoppings):
            for name, hop in self._PBlat.hoppings.items():
                for term in hop.terms:
                    output["Hoppings"].append({"Rel_Idx":term.relative_index[0:len(self._vec1)].tolist(),"From":keymap.get(str(term.from_id)),"To":keymap.get(str(term.to_id)),"Nrows":hop.energy.shape[0], "Ncols":hop.energy.shape[1],"ValsReal":np.real(hop.energy.ravel()).tolist(), "ValsImag":np.imag(hop.energy.ravel()).tolist(),"Kind":"Plain"})
                    
                    
                    
        else:   
            for f in self._FuncHoppings:
                if(not isinstance(f[3],np.ndarray)):
                    
                    output["Hoppings"].append({"Rel_Idx":f[0],"From":f[1],"To":f[2],"Nrows":np.array([f[3]]).shape[0], "Ncols":np.array([f[3]]).shape[0],"ValsReal":np.real(np.array([f[3]])).tolist(),"ValsImag":np.imag(np.array([f[3]])).tolist(),"Kind":"Func"})
                    
                    
                else:
                    output["Hoppings"].append({"Rel_Idx":f[0],"From":f[1],"To":f[2],"Nrows":np.array(f[3]).shape[0], "Ncols":np.array(f[3]).shape[1],"ValsReal":np.real(np.array(f[3]).ravel()).tolist(),"ValsImag":np.imag(np.array(f[3]).ravel()).tolist(),"Kind":"Func"})

        with open("./"+self._name+".prop/Hoppings.json","w") as f:
            json.dump(output,f,indent=4)

    ### Flush Operators
    ###Entonces Hay que iterar por operador de Forma que cada operador tenga su Onsite Y sus Hoppings para la construccion de los operadores en C++


    def FlushOperators(self):
        #primero hay que sacar el operador directamente
        if( not self._PBOperators):
            pass
        else:
            operators ={}### Sera el nombre del opeardor y luego se llena normalmente
            for OpName in self._PBOperators.keys():
                operators[str(OpName)] = {"Sublattices":[],"Hoppings":[]}
                #Ahora ponemos las subredes
                for name, sublat in self._PBOperators[str(OpName)].sublattices.items():
                    operators[str(OpName)]["Sublattices"].append({"Label":name,"Position": np.array(sublat.position,dtype=float).tolist(),"Nrows":sublat.energy.shape[0], "Ncols":sublat.energy.shape[1], "OnsiteReal":np.real(sublat.energy.ravel()).tolist(), "OnsiteImag":np.imag(sublat.energy.ravel()).tolist(), "Kind":"Plain"})

            ###JsonFlush
            for OpName in self._PBOperators.keys():
                with open("./"+self._name+".prop/"+str(OpName)+".json","w") as f:
                    json.dump(operators[str(OpName)],f,indent=4)

            
        

    ### Flush Disorder



    ### getters
    @property
    def Name(self):
        return self._name
    @property
    def Vec1(self):
        return self._vec1
    @property
    def Vec2(self):
        return self._vec2
    @property
    def Vec3(self):
        return self._vec3
    @property
    def PbLat(self):
        return self._PBlat
    @property
    def PbOperators(self):
        return self._PBOperators
    @property
    def FuncHoppings(self):
        return self._FuncHoppings
    @property
    def FuncSublat(self):
        return self._FuncSublat
    @property
    def AndersonDisorder(self):
        return self._AndersonDisorder
    @property
    def PuddleDisorder(self):
        return self._PuddleDisorder
    @property
    def VacancyDisorder(self):
        return self._VacancyDisorder
    @property
    def OperatorsDict(self):
        return self._PBOperators
    ###setters
    @Name.setter
    def Name(self, name):
        self._name = name
    @Vec1.setter
    def Vec1(self, vec1):
        self._vec1 = vec1
    @Vec2.setter
    def Vec2(self, vec2):
        self._vec2 = vec2
    @Vec3.setter
    def Vec3(self, vec3):
        self._vec3 = vec3
